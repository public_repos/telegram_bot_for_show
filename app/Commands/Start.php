<?php
/**
 * Created by PhpStorm.
 * User: kamron
 * Date: 21.03.19
 * Time: 1:26
 */

namespace App\Commands;

use App\Models\Data;
use App\Services\Keyboards\InlineKeyboard;
use App\Services\Keyboards\ReplyKeyboard;
use Telegram\Bot\Exceptions\TelegramOtherException;
use Telegram\Bot\Exceptions\TelegramResponseException;
use Telegram\Bot\Laravel\Facades\Telegram;

class Start
{

    public function init($hook){
        try{
            $selectLang = config('app.user_config.select_lang');
            $chat_id = $hook['message']['chat']['id'];
            $is_auth = $hook['auth'];

            $buttons = [
                [
                    [
                        "text" => "🇺🇿O'zbek tili",
                        "callback_data" => "change_lang_uz"
                    ],
                    [
                        "text" => "🇷🇺Русский",
                        "callback_data" => "change_lang_ru"
                    ]
                ]
            ];
            // Эти Keyboard классы возврашают готовый массив,
            // которвый можно передать в Telegram::sendMessage
            $msg = InlineKeyboard::emoji(false)->hideKeyboard(true)->getKeyboard(
                $chat_id,
                $selectLang['uz'] . "\n\n" . $selectLang['ru'],
                $buttons
            );

            try{
                Telegram::sendMessage($msg);
                Data::setState();
            } catch(TelegramResponseException $e){
                 return;
            }
        }
        catch (TelegramResponseException $e){
            return;
        }

    }
}
