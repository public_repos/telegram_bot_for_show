<?php
/**
 * Класс родитель для всех типов клавиатур.
 */

namespace App\Services\Keyboards;


use App\Http\Kernel;

abstract class Keyboard
{
    protected static $emoji = true;
    protected static $emojiCode = null;
    protected static $keyboard = [];
    protected static $chatId;
    protected static $text;
    public static $hideKeyboard = false;

    public static function getKeyboard(int $chat_id, string $text, array $keyboard)
    {
        static::$keyboard = $keyboard;
        static::$chatId = $chat_id;
        static::$text = $text;

        if (static::$emoji) {
            static::$keyboard = self::emojiConcat($keyboard);
        }

        return static::keyboardType();
    }

    private static function emojiConcat(array $keyboard) : array
    {
        $emojiKeyboard = self::makeKeyboardArray($keyboard);
        return $emojiKeyboard;
    }

    private static function makeKeyboardArray(array &$keyboard)
    {
        $returnKey = [];
        foreach ($keyboard as $key => $value) {
            foreach ($value as $k => $v) {
                if (is_array($v)) {
                    $returnKey[$key] = static::makeKeyboardArray($value);
                } else {
                    if ($k === 'text' && (isset($value['emoji']) || static::$emojiCode)) {
                        $returnKey[$key][$k] = trim((static::$emojiCode ? static::$emojiCode : $value['emoji'])) . ' ' . $v;
                    } else {
                        $returnKey[$key][$k] = $v;
                    }
                }
            }
        }

        return $returnKey;
    }

    public static function emojiCode(string $emojiCode)
    {
        static::$emojiCode = $emojiCode;
        return new static();
    }

    public static function emoji(bool $bool)
    {
        static::$emoji = $bool;
        return new static();
    }

    public static function hideKeyboard(bool $bool = false)
    {
        static::$hideKeyboard = $bool;
        return new static();
    }

    /**
     * Метод для определения своей клавиатуры
     */
    abstract protected static function keyboardType();
}
