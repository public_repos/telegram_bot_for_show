<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 2019-03-22
 * Time: 00:56
 */

namespace App\Services\Keyboards;


use Telegram\Bot\Laravel\Facades\Telegram;

class InlineKeyboard extends Keyboard
{

    protected static function keyboardType()
    {
        $keyboard = static::$keyboard;
        $replyKeyboard = [
            'inline_keyboard' => $keyboard
        ];

        $replyMarkup = Telegram::InlineKeyboardButton($replyKeyboard);

        if (static::$hideKeyboard) {
            $replyMarkup = json_decode($replyMarkup, true);
            $replyMarkup['reply_markup'] = [
                "hide_keyboard" => true,
                "selective" => false
            ];
            $replyMarkup = json_encode($replyMarkup);
        }

        $msg = [
            'chat_id' => static::$chatId,
            'text' => static::$text,
            'reply_markup' => $replyMarkup,
            'parse_mode' => 'html'
        ];

        static::$hideKeyboard = false;

        return $msg;
    }
}
