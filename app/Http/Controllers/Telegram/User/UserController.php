<?php

namespace App\Http\Controllers\Telegram\User;

use App\Models\User\TelegramUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    public function index()
    {
        return response()->json(TelegramUser::all());
    }

    public function store(array $data)
    {
        $user = TelegramUser::create($data);

        return response()->json($user, 201);
    }

    public function update($user_id, array $data)
    {
        $user = TelegramUser::find($user_id);
        $user->update($data);

        return response()->json($user, 201);
    }

    public function delete($user_id)
    {
        $user = TelegramUser::find($user_id);
        $user->delete();

        return response()->json(201);
    }

}
