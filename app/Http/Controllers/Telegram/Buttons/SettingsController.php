<?php

namespace App\Http\Controllers\Telegram\Buttons;

use App\Http\Controllers\Telegram\Card\CardController;
use App\Http\Controllers\Telegram\CommandController;
use App\Http\Controllers\Telegram\TelegramController;
use App\Models\Data;
use App\Models\User\TelegramUser;
use App\Services\Keyboards\ReplyKeyboard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Telegram\Bot\Exceptions\TelegramResponseException;
use Telegram\Bot\Laravel\Facades\Telegram;

class SettingsController extends Controller
{
    public function page(array $params = [])
    {
        $state = json_encode([
            "settings" => [
                "method" => "action"
            ]
        ]);

        $keyboard = Data::getSettingsMenu();

        $text = Data::getSettingText();

        $msg = ReplyKeyboard::emoji(false)->hideKeyboard(false)->getKeyboard(
            Data::getChatId(),
            $text,
            $keyboard
        );

        try {
            Telegram::sendMessage($msg);
            Data::setState($state);
        } catch (TelegramResponseException $e) {
            info($e);
            return;
        }

    }

    public function action($text)
    {
        /*if($text == Data::getUserConfig('text_main_menu')){

            Data::getMain($text);
            return;
        }*/ // Этот кусок кода не нужен так как в RouteController есть проверка на эту кнопку

        if($text == Data::getUserConfig('lang')){
            CommandController::init(TelegramController::$hook, true);
            return;
        }
        elseif($text == Data::getUserConfig('clear')){
            $user = TelegramUser::find(TelegramController::$hook['user_id']);
            (new CardController)->deleteAll($user);
            Data::getMain(Data::getUserConfig('clear_text'));
            Data::setState();
            return;
        }
    }
}
