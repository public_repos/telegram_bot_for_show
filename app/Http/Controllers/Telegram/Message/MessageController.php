<?php

namespace App\Http\Controllers\Telegram\Message;

use App\Models\User\TelegramUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
    private static $state;

    public static function init($hook)
    {
        self::parseMessage($hook);

        return self::$state;
    }


    public static function parseMessage($hook)
    {
        $user = '';
        if (isset($hook['user_id'])) {
            $user = TelegramUser::find($hook['user_id']);
        }

        $message = $hook['message'];
        $text = isset($message['text']) ? $message['text'] : false;

        if(isset($message['entities']) && $message['entities'][0]['type'] === 'bot_command'){
            self::$state = 'command';
        }

        elseif(
            (!isset($user->state) || empty($user->state->state)) &&
            (($text && mb_strpos($text, '998') !== false && mb_strlen($text) == 12) ||
            (isset($message['contact']) && isset($message['contact']['phone_number']))) ||
            (($text && mb_strpos($text, '+998') !== false && mb_strlen($text) == 13))
        ){
            self::$state = 'phone';
        }
    }
}
