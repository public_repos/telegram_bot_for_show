<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Telegram\TelegramController;
use App\Models\Data;
use App\Models\User\TelegramUser;
use Closure;

class tgAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $chat_id = 0;
        $input = $request->all();


        if (isset($input['message'])) {
            $chat_id = $input['message']['from']['id'];
        }
        else if (isset($input['callback_query'])) {
            $chat_id = $input['callback_query']['from']['id'];
        }

        $user = TelegramUser::where('chat_id', $chat_id)->first();

        if ($user) {
            $input['auth'] = true;
            $input['user_name'] = boolval($user->name) ?? false;
            $input['user_phone'] = boolval($user->phone) ?? false;
            $input['user_id'] = $user->id;
            $input['lang'] = $user->lang;
            $input['role'] = $user->role;
        }
        else{
            $input['auth'] = false;
        }



        $request->replace($input);

        TelegramController::$hook = $input;
        Data::$hook = $input;

        return $next($request);
    }
}
