<?php

namespace App\Models\User;

use App\Models\State\State;
use Illuminate\Database\Eloquent\Model;

class TelegramUser extends Model
{
    protected $table = 's_users';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'chat_id',
        'role',
        'name',
        'last_name',
        'user_name'
    ];


    public function state()
    {
        return $this->hasOne(State::class,'chat_id', 'chat_id');
    }

    public static function boot()
    {
        parent::boot();

        static::deleted(function ($user) {

        });
    }
}
