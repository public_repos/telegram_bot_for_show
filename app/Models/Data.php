<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 2019-03-23
 * Time: 18:51
 */

namespace App\Models;


use App\Http\Controllers\Telegram\TelegramController;
use App\Models\State\State;
use App\Services\Keyboards\ReplyKeyboard;
use Telegram\Bot\Exceptions\TelegramResponseException;
use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Этот класс хранит глобальные данные или методы
 */

class Data
{
    public static $hook;
    private static $lang = "";
    private static $chat_id = 0;
    private static $message_id = 0;

    public static $QIWI_RUB = "QIWI RUB";
    public static $QIWI_USD = "QIWI USD";
    public static $UZCARD = "UZCARD";

    public static function setMessageId(int $message_id)
    {
        static::$message_id = $message_id;
    }

    public static function getMessageId()
    {
        return static::$message_id;
    }

    public static function setChatId(int $chat_id)
    {
        static::$chat_id = $chat_id;
    }

    public static function getChatId(): int
    {
        return static::$chat_id;
    }

    public static function setUserLang(string $lang)
    {
        static::$lang = $lang;
    }

    public static function getUserLang()
    {
        if (empty(static::$lang)) {
            static::setUserLang(TelegramController::$hook['lang']);
        }
        return static::$lang;
    }

    public static function getUserConfig(string $item)
    {
        if (empty(static::$lang)) {
            static::setUserLang(TelegramController::$hook['lang']);
        }
        return config('tgconfig.user_config.' . $item . '.' . static::$lang);
    }

    public static function getPattern(): string
    {
        return static::getUserConfig('pattern_emoji');
    }

    public static function getMainMenu()
    {
        $buttons = self::getUserConfig('page_buttons');
        $keyboard = [
            [
                $buttons['order']
            ],
            [
                $buttons['cards'],
                $buttons['settings']
            ],
            [
                $buttons['blog'],
                $buttons['rate']
            ]
        ];

        if(TelegramController::$hook['role'] == 'admin'){
            array_unshift($keyboard, [$buttons['admin']]);
        }


        return $keyboard;
    }

    public static function getContactMenu()
    {
        $buttons = self::getUserConfig('phone_button');
        $keyboard = [
            [
                [
                    'text' => $buttons['phone'],
                    'request_contact' => true
                ]
            ]
        ];

        return $keyboard;
    }

    public static function getSettingsMenu()
    {
        $keyboard = [
            [self::getUserConfig('clear')],
            [self::getUserConfig('lang')],
            [self::getUserConfig('text_main_menu')]
        ];

        return $keyboard;
    }

    public static function getSettingText()
    {
        return self::getUserConfig('settings');
    }

    public static function getAdminMenu()
    {
        $keyboard = [
            ['Курсы', 'Резерв'],
            ['Заявки', 'Карты'],
            [self::getUserConfig('text_main_menu')],
        ];

        return $keyboard;
    }

    public static function getCards()
    {
        $buttons = self::getUserConfig('page_buttons');

        return [
            "qiwi" => $buttons['qiwi'],
            "uzcard" => $buttons['uzcard'],
        ];
    }

    public static function getMenuButton()
    {
        return static::getUserConfig('back_menu');
    }

    public static function getMain(string $text)
    {
        $msg = ReplyKeyboard::emoji(false)->getKeyboard(
            Data::getChatId(),
            $text,
            Data::getMainMenu()
        );

        try{
            Telegram::sendMessage($msg);
        } catch(TelegramResponseException $e){
            info($e);
            return;
        }
    }

    public static function getQiwiPattern()
    {
        return config('app.user_config.qiwi_pattern');
    }

    public static function getUzCardPattern()
    {
        return config('app.user_config.uzcard_pattern');
    }

    public static function setState($state = '')
    {
        State::updateOrInsert(
            ['chat_id' => Data::getChatId()],
            ['state' => $state]
        );
    }

    public static function getConvertData($from, $to)
    {
        return json_encode([
            "order" => [
                "currency_convert" => [
                    "from" => $from,
                    "to" => $to
                ]
            ]
        ]);
    }

    public static function getConvertTo($currency_1, $currency_2)
    {
        return $currency_1 . "_TO_" . $currency_2;
    }
}
