<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBotTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('chat_id');
            $table->string('role', 255)->default('user');
            $table->string('name', 255);
            $table->string('last_name', 255)->nullable();
            $table->string('user_name', 255)->nullable();

            $table->timestamps();
        });

        Schema::create('s_user_settings', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->string('phone')->index();
            $table->boolean('verified')->default(0);
            $table->string('lang', 255)->default('ru');
            $table->string('bonus')->default(0);

            $table->foreign('user_id')->references('id')->on('s_users');
            $table->timestamps();
        });

        Schema::create('s_points', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('s_tariffs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tm_id');
            $table->string('name', 255);
            $table->text('text');
            $table->string('filename');

            $table->timestamps();
        });

        Schema::create('s_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('tm_order_id');
            $table->unsignedInteger('point_id');
            $table->unsignedInteger('tariff_id');
            $table->boolean('is_pre_order')->default(0);
            $table->string('bonus_used')->default(0);
            $table->text('comment')->nullable();


            $table->foreign('user_id')->references('id')->on('s_users');
            $table->foreign('tariff_id')->references('id')->on('s_tariffs');
            $table->foreign('point_id')->references('id')->on('s_points');

            $table->timestamps();
        });

        Schema::create('s_order_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('order_id');
            $table->integer('value');

            $table->foreign('order_id')->references('id')->on('s_orders');
            $table->timestamps();
        });

        Schema::create('s_user_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('point_id');
            $table->string('name', 255);

            $table->foreign('user_id')->references('id')->on('s_users');
            $table->foreign('point_id')->references('id')->on('s_points');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_user_addresses');
        Schema::dropIfExists('s_order_statuses');
        Schema::dropIfExists('s_orders');
        Schema::dropIfExists('s_tariffs');
        Schema::dropIfExists('s_points');
        Schema::dropIfExists('s_user_settings');
        Schema::dropIfExists('s_users');
    }
}
